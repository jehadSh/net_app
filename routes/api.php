<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\GroupController;
use App\Http\Controllers\LogController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Route::get("/test/{id}", [FileController::class, 'downloadFile']);
Route::get("/test", [FileController::class, 'test']);


Route::post("/register", [AuthController::class, 'register']);
Route::post("/login", [AuthController::class, 'login']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::post("/logout", [AuthController::class, 'logout']);
    Route::get("/isToken", [AuthController::class, 'isToken']);

    Route::resource('groups', GroupController::class);
    Route::resource('files', FileController::class);

    Route::group(['prefix' => 'group'], function () {
        Route::get("/my_groups", [GroupController::class, 'myGroups']);
        Route::get("/join_group", [GroupController::class, 'joinGroup']);
        Route::get("/search/{name}", [GroupController::class, 'search']);
    });
    Route::group(['prefix' => 'file'], function () {
        Route::get("/download_file/{id}", [FileController::class, 'downloadFile']);
        Route::post("/upload_new/{id}", [FileController::class, 'uploadNew']);
        Route::put("/check_in/{id}", [FileController::class, 'checkIn']);
        Route::put("/bulk_check_in", [FileController::class, 'bulkCheckIn']);
        Route::put("/check_out/{id}", [FileController::class, 'checkout']);
    });

    Route::group(['prefix' => 'log'], function () {
        Route::get("/file_log/{id}", [LogController::class, 'fileLog']);
        Route::get("/group_log/{user_id}/{group_id}", [LogController::class, 'groupLog']);
    });
});
