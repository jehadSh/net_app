<?php

namespace Database\Seeders;

use App\Models\Group;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CreateGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $group = [
            [
                'name' => 'first group',
                'owner_id' => '1',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
            [
                'name' => 'secound group',
                'owner_id' => '2',
                'created_at' => '2021-10-12',
                'updated_at' => '2021-10-12',
            ],
        ];

        Group::insert($group);
    }
}