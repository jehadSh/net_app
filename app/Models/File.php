<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;

    protected $guarded = [];

    // public function created()
    // {
    //     return $this->belongsTo(User::class, 'created_by');
    // }

    // public function inGroup()
    // {
    //     return $this->belongsTo(Group::class, 'group_id');
    // }

    // public function booked()
    // {
    //     return $this->belongsTo(User::class, 'booked_by');
    // }
    public function log()
    {
        return $this->hasMany(Log::class, 'file_id');
    }
    public function tt()
    {
        return $this->belongsTo(Group::class, 'group_id');
    }
}