<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $appends = ['owner_name'];


    public function users()
    {
        return $this->belongsToMany(User::class);
    }
    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    // public function my_owner()
    // {
    //     return $this->belongsTo(User::class, 'owner_id');
    // }

    public function files()
    {
        return $this->hasMany(File::class, 'group_id');
    }

    public function getOwnerNameAttribute()
    {
        return $this->owner()->pluck('name')->first();
    }


}