<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $appends = ['owner_name', 'file_name'];


    public function owner()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function files()
    {
        return $this->belongsTo(File::class, 'file_id');
    }

    public function getOwnerNameAttribute()
    {
        return $this->owner()->pluck('name')->first();
    }
    public function getFileNameAttribute()
    {
        return $this->files()->pluck('name')->first();
    }
}
