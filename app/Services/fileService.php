<?php

namespace App\Services;

use App\Models\File;
use App\Models\Log;
use Exception;
use Illuminate\Support\Facades\DB;

class FileService
{
    // public function checkIn($file_id, $booked_by)
    // {
    //     $file = File::where('id', $file_id)->first();
    //     try {
    //         DB::beginTransaction();
    //         if (!$file->booked_by) {
    //             $file->sharedLock();
    //             $file->update([
    //                 'booked_by' => $booked_by,
    //             ]);
    //             DB::commit();
    //             return response()->json($file);
    //         } else {
    //             return response()->json("no file avabial");
    //         }
    //     } catch (\Exception $e) {
    //         DB::rollBack();
    //         throw $e;
    //     }

    // }


    public function checkIn($file_id, $booked_by)
    {
        try {
            DB::beginTransaction();
            $file = File::where('id', $file_id)->first();
            if (!$file) {
                throw new Exception('File is not exist actually');
            }
            if (!$file->booked_by) {
                $file->lockForUpdate();
                $file->update([
                    'booked_by' => $booked_by,
                ]);
                $log = new Log();
                $log->file_id = $file_id;
                $log->created_by = $booked_by;
                $log->action = 'check_In';
                $log->save();

            } else {
                throw new Exception('The file is booked');
            }
            DB::commit();
            return response()->json($file);
        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }

}
