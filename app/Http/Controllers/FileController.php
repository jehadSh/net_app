<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Services\FileService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Log;



class FileController extends Controller
{
    public $fileService;
    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    public function index()
    {
        //
    }
    public function store(Request $request)
    {
        $created_by = Auth::user()->id;
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $file_path = uniqid() . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public/files', $file_path);
            $my_file = File::create([
                'name' => $request->name ?? "12",
                'path' => $file_path,
                'created_by' => $created_by,
                // 'created_by' => 1,
                'group_id' => $request->group_id ?? 1,
            ]);
            return response()->json($my_file);
        }
        return response()->json('No file uploaded');
    }
    public function show($id)
    {
        $file = File::where('id', $id)->first();
        if (!$file) {
            return response()->json('It does not exist actually', 200);
        } else {
            return response()->json($file);
        }
    }
    public function update(Request $request, $id)
    {
        // $booked_by = Auth::user()->id;
        // $file = File::where('id', $id)->first();
        // if (!$file) {
        //     return response()->json('It does not exist actually');
        // } else if (!$file->booked_by) {
        //     return response()->json('hiiiiiii');
        // } else {
        //     $file->update([
        //         'booked_by' => $booked_by,
        //     ]);
        //     return response()->json($file);
        // }
        return response()->json("what you need man ?");

    }
    public function destroy($id)
    {
        $file = File::where('id', $id)->first();
        if (!$file) {
            return response()->json('It does not exist actually');
        } else {
            $path = storage_path('app/public/files/') . $file->path;
            unlink($path);
            $file = $file->delete();
            return response()->json('Done Delete file');
        }
    }
    public function downloadFile($id)
    {
        $file = File::where('id', $id)->first();
        if (!$file) {
            return response()->json('It does not exist actually', 200);
        } else {
            $path = storage_path('app/public/files/') . $file->path;

            if (file_exists($path)) {
                return response()->file($path);
            } else {
                return response()->json(['message' => 'File not found'], 404);
            }
        }


    }
    public function uploadNew(Request $request, $id)
    {
        $file = File::where('id', $id)->first();
        if (!$file) {
            return response()->json('It does not exist actually');
        } else {
            $path = storage_path('app/public/files/') . $file->path;
            unlink($path);
            if ($request->hasFile('file')) {
                $new_file = $request->file('file');
                $file_path = uniqid() . '.' . $new_file->getClientOriginalExtension();
                $new_file->storeAs('public/files', $file_path);
                $file->update([
                    'name' => $request->name ?? $file->name,
                    'path' => $file_path,
                ]);
                return response()->json($file);
            } else {
                return response()->json('not have file');
            }
        }
    }
    public function checkIn($file_id)
    {
        try {

            $booked_by = Auth::user()->id;
            return $this->fileService->checkIn($file_id, $booked_by);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 403);
        }
    }

    public function bulkCheckIn(Request $request)
    {
        $booked_by = Auth::user()->id;
        try {
            DB::beginTransaction();
            foreach ($request->ids as $file_id) {
                $this->fileService->checkIn($file_id, $booked_by);

            }
            DB::commit();
            return response()->json('files check in done successfully ');
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($e->getMessage(), 403);
        }
    }
    public function checkout($id)
    {
        $created_by = Auth::user()->id;
        $file = File::where('id', $id)->first();
        if (!$file) {
            return response()->json('It does not exist actually');
        } else {
            $file->update([
                'booked_by' => null,
            ]);
            $log = new Log();
            $log->file_id = $id;
            $log->created_by = $created_by;
            $log->action = 'check_Out';
            $log->save();
            return response()->json($file);
        }
    }
}
