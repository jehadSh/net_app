<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class GroupController extends Controller
{

    public function index()
    {
        $group = Group::where('id', ">", 0)->with('users')->get();
        // $group = Group::with('uesrs')->get();

        return response()->json($group, 200);
    }
    public function store(Request $request)
    {

        $owner_id = Auth::user()->id;

        $group = new Group();
        $group->name = $request->name;
        $group->owner_id = $owner_id;
        $group->save();

        $users = User::whereIn('id', $request->members_id)->get()->pluck('id');
        $group->users()->sync($users);

        return response()->json($group, 201);
    }

    public function show($id)
    {
        $group = Group::where('id', $id)->with('users')->with('files')->first();

        return response()->json($group);
    }

    public function update(Request $request, $id)
    {
        $group = Group::where('id', $id)->first();
        if (!$group) {
            return response()->json('It does not exist actually', 200);
        } else {
            $group->update([
                'name' => $request->name ?? $group->name
            ]);
            if ($request->members_id) {
                $users = User::whereIn('id', $request->members_id)->get()->pluck('id');
                $group->users()->sync($users);
                // $group->users()->syncWithoutDetaching($users);
            }
        }


        return response()->json($group);
    }

    public function destroy($id)
    {
        $group = Group::where('id', $id)->first();
        if (!$group) {
            return response()->json('It does not exist actually', 200);
        } else {
            $group = $group->delete();
        }
        // $this->authorize('delete', $form);


        return response()->json('Done Delete Group', 200);
    }

    public function myGroups()
    {
        $owner_id = Auth::user()->id;

        $my_group = Group::where('owner_id', $owner_id)->get();
        return response()->json($my_group);
    }

    public function joinGroup()
    {
        $my_id = Auth::user()->id;

        $join_group = Group::whereHas('users', function ($query) use ($my_id) {
            $query->where('user_id', $my_id);
        })->get();


        return response()->json($join_group);
    }

    public function search($name)
    {
        $user = User::where('name', 'LIKE', "%$name%")->get();
        return response()->json($user);

    }
}
