<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log;


class LogController extends Controller
{
    public function fileLog($file_id)
    {
        $log = Log::where('file_id', $file_id)->get();
        return response()->json($log);
    }

    public function groupLog($user_id, $group_id)
    {
        $logUser = Log::where('created_by', $user_id)
            ->whereHas('files', function ($query) use ($group_id) {
                // Apply constraints on the related model
                $query->where('group_id', $group_id);
            })
            ->get();
        return response()->json($logUser);

    }
}
